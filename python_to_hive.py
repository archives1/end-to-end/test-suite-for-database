
from dotenv import load_dotenv
import os
load_dotenv('./.env')

# from pyhive import presto  # or import hive or import trino
from pyhive import hive  # or import hive or import trino


cursor = hive.connect(os.environ["DAP_HOST"]).cursor()

# conn = hive.Connection(host=os.environ["DAP_HOST"]
#                        ,port=10000
#                        ,auth="cloudera"
#                        ,database="default"
#                        ,kerberos_service_name="hive"
#                        )
# cursor = conn.cursor()

cursor.execute('SELECT * FROM my_awesome_data LIMIT 10')
print((cursor.fetchone()))
print((cursor.fetchall()))



