
*** Settings ***
Library    Collections
Library    ExcelLibrary


*** Variables ***
# ${CURRENT_PATH}    /home/sun/test-suite-01

*** Test Cases ***
Test scenario 010101
    Save Result To Excel File

*** Keywords ***
Save Result To Excel File
    ${row1}=    Create List    Labal1    value1
    ${row2}=    Create List    Labal2    value2
    ${DATA_TABLE}=    Create List    ${row1}    ${row2}
    ${CURRENT_DOC}=    Create Excel Document    doc_id=doc0101
    Write Excel Rows    rows_data=${DATA_TABLE}    rows_offset=1    col_offset=0    sheet_name=Sheet
    Save Excel Document    filename=./test_scenario_0101.xlsx
    Close All Excel Documents
